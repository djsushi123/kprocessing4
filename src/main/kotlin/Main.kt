import math.*

var scale = 0.0

var t = 0.0

fun main() = kpapplet(width = 500, height = 500) {

    setup {
        surface.setResizable(true)

        frameRate(60f)

        stroke(255)
        strokeWeight(4f)
    }

    draw {
        background(0)
        scale = if (width > height) height / 500.0 * 50 else width / 500.0 * 50

        val a = 0.3 * sin(t) + 1.0
        val b = 2.0
        val m = 6.0
        val n1 = 1.0
        val n2 = 0.5 * sin(t) + 1
        val n3 = cos(t) + 1

        pushMatrix()
        translate(width / 2f, height / 2f)
        noFill()
        beginShape()
        var theta = 0.0
        while (theta <= 2*PI) {
            val rad = r(theta, a, b, m, n1, n2, n3)
            val x = rad * cos(theta) * scale
            val y = rad * sin(theta) * scale
            vertex(x, y)

            theta += 0.001
        }
        endShape()
        popMatrix()

        // text overlay
        fill(255)
        textAlign(LEFT)
        textSize(scale.float / 3.5f)
        text("""
            a = $a
            b = $b
            m = $m
            n1 = $n1
            n2 = $n2
            n3 = $n3
        """.trimIndent(), 10f, 20f + scale.float * 0.1f)

        t += 0.05
    }
}

fun r(theta: Double, a: Double, b: Double, m: Double, n1: Double, n2: Double, n3: Double) =
    pow(
        pow(
            abs(cos(m * theta / 4.0) / a), n2) + pow(abs(sin(m * theta / 4.0) / b), n3
        ),
        -1.0 / n1
    )