package math

fun sin(angle: Double) = kotlin.math.sin(angle)

fun cos(angle: Double) = kotlin.math.cos(angle)

fun tan(angle: Double) = kotlin.math.tan(angle)

fun asin(angle: Double) = kotlin.math.asin(angle)

fun acos(angle: Double) = kotlin.math.acos(angle)

fun atan(angle: Double) = kotlin.math.atan(angle)