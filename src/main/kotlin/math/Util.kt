package math

import float
import processing.core.PApplet
import kotlin.math.pow

fun abs(n: Double) = kotlin.math.abs(n)

fun pow(n: Double, e: Double) = n.pow(e)

fun map(value: Double, start1: Double, stop1: Double, start2: Double, stop2: Double) =
    PApplet.map(value.float, start1.float, stop1.float, start2.float, stop2.float).toDouble()