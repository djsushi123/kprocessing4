// toFloat() shortening
val Number.float
    get() = toFloat()