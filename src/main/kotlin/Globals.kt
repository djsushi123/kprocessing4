import kprocessingenvironment.KProcessingEnvironment

object Globals {

    // main instance of kprocessingenvironment.KProcessingEnvironment
    lateinit var mainEnvironment: KProcessingEnvironment

    // here, we store the reference to the user's setup() lambda
    lateinit var setupFunction: KProcessingEnvironment.() -> Unit

    // the user's draw() lambda
    lateinit var drawFunction: KProcessingEnvironment.() -> Unit

}