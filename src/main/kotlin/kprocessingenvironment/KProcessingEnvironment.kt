package kprocessingenvironment

import Globals
import float
import processing.core.PApplet

open class KProcessingEnvironment(val screenWidth: Int, val screenHeight: Int) : PApplet() {

    override fun settings() {
        size(screenWidth, screenHeight, P3D)
    }

    override fun setup() {
        Globals.setupFunction(Globals.mainEnvironment)
    }

    override fun draw() {
        Globals.drawFunction(Globals.mainEnvironment)
    }

    fun scale(s: Double) = scale(s.float)

    fun ellipse(a: Number, b: Number, c: Number, d: Number) {
        super.ellipse(a.float, b.float, c.float, d.float)
    }

    fun vertex(x: Double, y: Double) {
        vertex(x.float, y.float)
    }

    companion object
}