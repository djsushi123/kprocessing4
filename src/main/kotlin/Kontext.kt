import kprocessingenvironment.KProcessingEnvironment

object Kontext {

    fun setup(setupEnvironment: KProcessingEnvironment.() -> Unit) {
        Globals.setupFunction = setupEnvironment
    }

    fun draw(drawEnvironment: KProcessingEnvironment.() -> Unit) {
        Globals.drawFunction = drawEnvironment
    }

}