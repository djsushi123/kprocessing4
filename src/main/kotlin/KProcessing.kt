import kprocessingenvironment.KProcessingEnvironment
import processing.core.PApplet

fun kpapplet(
    width: Int,
    height: Int,
    program: Kontext.() -> Unit
) {
    program(Kontext)
    val processingArgs = arrayOf("")
    Globals.mainEnvironment = KProcessingEnvironment(width, height)
    PApplet.runSketch(processingArgs, Globals.mainEnvironment)
}